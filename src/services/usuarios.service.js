const md5 = require('md5');
const jwt = require('jsonwebtoken');
const hashSecret = process.env.CRYPTO_KEY;

const buscarPorEmail = async (email) => {

  const usuario = [{
    id: 1, 
    nome: 'teste' ,
    email: 'teste@email.com', 
    tipo: "adm", 
    datanascimento: "29/10/1986"
  }, 
  {

  }]
}

const criarHash = (senha) => {
  return md5(senha + hashSecret);
}

const buscaUsuario = async (usuario, senha) => {

  return true;

};

const criaCredencial = async (usuarioEmail) => {

  try {

    const usuario = {

      id: 1, nome: 'teste' , email: 'teste@email.com', tipo: "adm", datanascimento: "29/10/1986"

    }

    const { id, nome, email, tipo, datanascimento } = usuario;

    const credencial = {
      token: jwt.sign({ email: usuario.email }, process.env.JWT_KEY, {
        expiresIn: `${process.env.JWT_VALID_TIME}ms`,
      }),
      usuario: {
        id,
        nome,
        email,
        tipo,
        datanascimento
      }
    }

    return credencial;

  } catch (error) {
    console.log(error);
  }
}

const validaEmailExistente = async (email, id = 0) => {

  const resul = await buscaPorEmail(email);

  if (id === 0) {
    return resul ? true : false;
  }

  if (resul) {
    if (resul.id === id)
      return false;

    return true;
  } 
  else {
    return false;
  }
}

const criaAluno = async (model) => {

  const modelParaCadastro = {
    nome: model.nome,
    email: model.email,
    tipo: '2',
    datanascimento: model.datanascimento,
    senha: criarHash(model.senha),
  };

  return usuarios.create(modelParaCadastro);
}

const alteraAluno = async (id, model) => {
  return usuarios.update(
    {
      nome: model.nome,
      email: model.email,
      datanascimento: model.datanascimento,
    },
    {
      where: { id: id }
    }
  )
}

const pesquisaAlunoId = async (idAluno) => {

  const itemDB = await usuarios.findOne({
    where: {
      id: idAluno
    },
    include: {
      model: inscricoes,
      as: 'inscricoes',
      include: {
        model: cursos,
        as: 'curso',
      }
    }
  });

  const { id, nome, email, datanascimento } = itemDB;

  const resultInscricoes = itemDB.inscricoes.map(itemInscricao => {

    const { id, curso } = itemInscricao;

    return {
      id,
      curso: {
        id: curso.id,
        name: curso.name
      }
    }

  });

  return {
    id,
    nome,
    email,
    datanascimento,
    qtd_inscricoes: resultInscricoes.length,
    inscricoes: resultInscricoes
  }
}

const listar = async (tipo) => {

  let where = {};

  if (tipo)
    where = {
      tipo
    }

  const reuslFromDB = await usuarios.findAll({
    where,
    include: [
      {
        model: inscricoes,
        as: 'inscricoes',
        include: [
          {
            model: cursos,
            as: 'curso'
          }
        ]
      }
    ],
  });

  return reuslFromDB;
}

const listarAlunos = async () => {

  const resultFromDB = await listar('2');

  return resultFromDB.map(item => {

    const { id, email, nome, tipo, inscricoes } = item;

    return {
      id,
      nome,
      email,
      tipo,
      inscricoes: inscricoes.reduce((acc, item) => {
        const { id, curso } = item;
        const novoItem = { id, curso: curso.name, };
        return [...acc, novoItem]
      }, []),

    }
  });
}

const listarTudo = async () => {

  const reuslFromDB = await listar();

  return reuslFromDB.map(item => {

    const { id, email, nome, tipo, inscricoes } = item;

    return {
      id,
      nome,
      email,
      tipo,
      inscricoes: inscricoes.reduce((acc, item) => {
        const { id, curso } = item;
        const novoItem = { id, curso: curso.name, };
        return [...acc, novoItem]
      }, []),

    }
  });
}

module.exports = {
  buscaUsuario,
  criaAluno,
  criaCredencial,
  validaEmailExistente,
  buscarPorEmail,
  alteraAluno,
  listarAlunos,
  pesquisaAlunoId,
  listarTudo,
}
