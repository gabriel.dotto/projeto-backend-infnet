// const { inscricoes, } = require("../models");


const validaSeInscricaoExiste = async (inscricaoid) => {

    const resultFromDB = await inscricoes.findOne({
      where: {
        id: inscricaoid
      }  
    })  
    return resultFromDB ? true : false;  
  }
  
  const validaSeInscricaoPertenceAoUsuario = async (inscricaoId, usuarioId) => {
  
    const resultFromDB = await inscricoes.findOne({
      where: {
        id: inscricaoId
      }
    });
  
    return resultFromDB.usuario_id === usuarioId ? true : false;  
  }
  
  const validaSeUsuarioJaInscrito = async (cursoId, usuarioId) => {
  
    const resultFromDB = await inscricoes.findOne({
      where: {
        curso_id: cursoId,
        usuario_id: usuarioId
      }
    });
  
    return resultFromDB ? true : false;  
  }
  
  const removerInscricao = async (cursoId) => {
  
    return inscricoes.destroy({
      where: {
        id: cursoId
      }
    });  
  }
  
  module.exports = {
    removerInscricao,
    validaSeUsuarioJaInscrito,
    validaSeInscricaoExiste,
    validaSeInscricaoPertenceAoUsuario,
  }
  