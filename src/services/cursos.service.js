const { cursos } = require("../models");

const { BusinessError } = require('../utils/errors/business.error')
const listarPorPerfilUsuario = async (usuarioTipo, usuarioId) => {

  const resultFromDB = await cursos.findAll({
    include: [
      {
        model: inscricoes,
        as: 'inscricoes',
      }
    ]
  });

  let result = [];

  if (Number(usuarioTipo) === 2) {

    result = resultFromDB.map((item) => {

      const { id, name, start_date, status, inscricoes } = item;

      const filterResult = inscricoes.filter((itemFilter) => {
        return Number(itemFilter.usuario_id) === Number(usuarioId)
      });

      return {
        id,
        name,
        start_date,
        status,
        inscrito: filterResult.length > 0 ? true : false
      };

    });

  } else {

    result = resultFromDB.map(item => {

      const { id, name, start_date, status, inscricoes } = item;

      return {
        id,
        name,
        start_date,
        status,
        qtd_inscricoes: inscricoes.length
      }

    });
  }
  return result;
}

const pesquisarCursoPorPerfilUsuario = async (id, usuarioId, usuarioTipo) => {

  const resultFromDB = await cursos.findOne({
    where: {
      id: id
    },
    include: [
      {
        model: inscricoes,
        as: 'inscricoes',
        include: [
          {
            model: usuarios,
            as: 'usuario',
          }
        ]
      },
    ]
  });

  let inscricoesFiltered = resultFromDB.inscricoes;

  if (Number(usuarioTipo) === 2) {
    inscricoesFiltered = inscricoesFiltered.filter(item => item.usuario_id === usuarioId);
  }

  const inscricoesFinal = inscricoesFiltered.map((itemInscricao) => {
    return {
      id: itemInscricao.id,
      aluno: {
        id: itemInscricao.usuario.id,
        nome: itemInscricao.usuario.nome,
        email: itemInscricao.usuario.email,
        datanascimento: itemInscricao.usuario.data_nascimento,
      }
    }
  });

  return {
    id: resultFromDB.id,
    name: resultFromDB.name,
    coordinator: resultFromDB.coordinator,
    start_date: resultFromDB.start_date,
    status: resultFromDB.status,
    inscricoes: inscricoesFinal
  }
}

const validaSeNomeExiste = async (nome) => {

  var resultFromDB = await cursos.findOne({
    where: {
      name: Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('name')), 'LIKE', nome.toLowerCase())
    }
  });

  return resultFromDB ? true : false;
}

const criaCurso = async (body) => {
  return cursos.create({
    name: body.name,
    coordinator: body.coordinator,
    start_date: `${body.start_date}`
  });
}

const alteraCurso = async (id, body) => {

  const model = {
    name: body.name,
    coordinator: body.coordinator,
    start_date: body.start_date,
  }

  return cursos.update(
    { ...model },
    { where: { id: id } }
  )
}

const deletaCurso = async (id) => {

  return cursos.destroy({
    where: {
      id: id
    }
  });
}

const validaCursoExistente = async (id) => {

  const resultFromDB = await cursos.findOne({
    where: {
      id
    }
  })

  if (!resultFromDB)
    throw new BusinessError('Curso informado inexistente!')

  return true;
}

const addInscricaoAluno = async (cursoId, usuarioId) => {

  const resultFromDB = await inscricoes.find({
    curso_id: cursoId,
    usuario_id: usuarioId,
  })

  if (resultFromDB) {
    throw new BusinessError("Aluno já existe!")
  }

  return inscricoes.create({
    curso_id: cursoId,
    usuario_id: usuarioId,
  });
}

module.exports = {
  addInscricaoAluno,
  alteraCurso,
  criaCurso,
  deletaCurso,
  validaSeNomeExiste,
  validaCursoExistente,
  listarPorPerfilUsuario,
  pesquisarCursoPorPerfilUsuario,
}
