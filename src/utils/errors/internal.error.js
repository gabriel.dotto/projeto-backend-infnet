const BusinessError = class InternalServerError extends Error {

    constructor(message, stack) {
        super(message, stack)
        this.statusCode = 400;
    }
}

module.exports.BusinessError = BusinessError
