const BusinessError = class BusinessError extends Error {

    constructor(message, stack = '') {
        super(message, stack)
        this.statusCode = 400;
    }
}

module.exports.BusinessError = BusinessError
