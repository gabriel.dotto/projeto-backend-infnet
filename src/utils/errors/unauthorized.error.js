exports.UnauthorizedError = class UnauthorizedError extends Error {

    constructor() {
        super('Usuário não autorizado', '')
        this.statusCode = 403;
    }
}
