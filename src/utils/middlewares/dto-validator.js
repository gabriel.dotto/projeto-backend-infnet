// const validateDto = (contexto, schema) => {

//     return (req, res, next) => {
//         const result = schema.validate(req[contexto]);
//         if (result.error) {
//             return res.status(400).json(result.error.details);
//         } 

//         return next();
//     }
// }

// module.exports.validateDto = validateDto;

const Joi = require('joi');
const jwt = require('jsonwebtoken');
const usuarioService = require('../../services/usuarios.service')
const { UnauthorizedError } = require('../../utils/errors/unauthorized.error')

const perfis = [
  {
    id: '1',
    funcionalidades: [
      'LISTAR_CURSO',
      'CRIAR_CURSO',
      'ALTERAR_CURSO',
      'DELETAR_CURSO',
    ]
  },
  {
    id: '2',
    funcionalidades: [
      'ALTERAR_ALUNO',
      'REALIZAR_INSCRICAO',
      'DELETAR_INSCRICAO',
    ]
  },
];

const criarDetalhes = (error) => {

  return error.details.reduce((acc, item) => {

    console.log(acc);

    console.log(item);

    return [
      ...acc, item.message
    ];
  }, []);
}

exports.errorHandler = () => {

  return (err, req, res, next) => {

      const statusCode = err.statusCode || 500

      if (statusCode != 500) {

        return res.status(err.statusCode).send({ 
          statusCode: err.statusCode,
          message: err.message
        });
      } 
    
      return res.status(statusCode).send({ 
        statusCode: statusCode,
        message: 'Internal Server Error'
      })
    }
}

exports.validarDTO = (type, params) => {

  return (req, res, next) => {
    try {
      const schema = Joi.object().keys(params);

      const { value, error } = schema.validate(req[type], {
        allowUnknown: false,
      });

      req[type] = value;

      return error ? res.status(400).send({
        detalhes: [...criarDetalhes(error)],
      }) : next();

    } catch (error) {
      console.log(error);
    }
  }
}

exports.autorizar = (rota = '*') => {

  return async (req, res, next) => {

    const { token } = req.headers;

    if (!token) {

      next(new UnauthorizedError());
    }

    const userJwt = jwt.verify(token, process.env.JWT_KEY);

    const usuario = await usuarioService.buscarPorEmail(userJwt.email);
    req.usuario = usuario;

    next();
  }
}
