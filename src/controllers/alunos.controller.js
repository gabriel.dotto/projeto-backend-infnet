const usuarioService = require('../services/usuarios.service');

const autenticacao = async (req, res, next) => {

  try {

    const { usuario, senha } = req.body;

    let result = await usuarioService.buscaUsuario(usuario, senha);

    if (!result) {
      return res.status(401).send({
        mensagem: "Usuário ou senha inválidos."
      })
    }

    var credencial = await usuarioService.criaCredencial(usuario);

    return res.status(200).send(credencial);

  } catch (error) {

    console.log(error);

    res.status(500).send({
      mensagem: "Internal server error",
    });
  }
}

const cadastroAluno = async (req, res, next) => {

  try {
    const { body } = req;

    const validacaoEmail = await usuarioService.validaEmailExistente(body.email);

    console.log(validacaoEmail);

    if (validacaoEmail) {
      return res.status(400).send({
        detalhes: [
          `"Este e-mail" já está cadastrado."`,
        ]
      });
    }

    await usuarioService.cadastroAluno(body);

    return res.status(200).send({
      mensagem: "Cadastro realizado com sucesso.",
    });

  } catch (error) {

    console.log(error);

    res.status(500).send({
      mensagem: "ERROR!",
    });
  }

}

const alteraAluno = async (req, res, next) => {

  const { body, params } = req;

  console.log('req.usuario.id: ', req.usuario.id);
  console.log('params.id: ', params.id);
  if (Number(params.id) !== Number(req.usuario.id))
    return res.status(400).send({
      mensagem: `"Operação nao permitida.`,
    });

  const validacaoEmail = await usuarioService.validaEmailExistente(body.email, params.id);

  if (validacaoEmail) {
    return res.status(400).send({
      mensagem: `"Este e-mail" já está cadastrado.`,
    });
  }

  await usuarioService.alteraAluno(params.id, body);


  return res.status(200).send({
    mensagem: "Alteração realizada com sucesso",
  });

}

const pesquisaAlunoId = async (req, res, next) => {

  try {
    const { params } = req;

    const user = await usuarioService.pesquisaAlunoId(params.id);

    return res.status(200).send(user);

  } catch (error) {

    console.log(error);

    return res.status(500).send({
      mensagem: "Internal server error",
    });

  }

}

const listarAlunos = async (req, res, next) => {

  const alunos = await usuarioService.listarAlunos();

  return res.status(200).send(alunos);

}

const listarUsuarios = async (req, res, next) => {

  const usuarios = await usuarioService.listarTudo();

  return res.status(200).send(usuarios);

}

module.exports = {
  autenticacao,
  cadastroAluno,
  alteraAluno,
  listarAlunos,
  pesquisaAlunoId,
  listarUsuarios
}
