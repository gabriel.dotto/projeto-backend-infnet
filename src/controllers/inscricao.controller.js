// const Joi = require('joi');

// const createParamsSchema = Joi.object({
//     id: Joi.number().required()
// });

// const createBodySchema = Joi.object({
//     datacriacao: Joi.string().required()
// });

// // const loginQuerySchema = Joi.object({
// //     testequery: Joi.string().required()
// // });

// module.exports.createParamsSchema = createParamsSchema;
// module.exports.createBodySchema = createBodySchema;

// module.exports.create = (req, res, next) => {

//     res.status(200).send('Serviço OK!');

// }

// const { cursos, inscricoes } = require("../models");

const cursosServices = require('../services/cursos.service');
const inscricoesServices = require('../services/inscricoes.service');
const { BusinessError } = require('../utils/errors/business.error');

const getCursos = async (req, res, next) => {
  
  const result = await cursos.findAll({});

  res.status(200).send(result.map(item => {

    const { id, name, ...rest } = item;

    return {
      id,
      name
    }

  }) || []);

}

const getCursoPorId = async (req, res, next) => {

  try {
    const { cursoid } = req.params

    const result = await cursos.findOne({
      where: {
        id: cursoid
      },
      include: {
        model: inscricoes,
        as: 'inscricoes',
      },
    });

    const data = {
      id: result.id,
      name: result.name,
      coordinator: result.coordinator,
      status: result.status,
      subscriptions: result.inscricoes,
    }

    res.status(200).send(data);


  } catch (error) {

    res.status(500).send({ message: 'Internal server error!!' });

  }
}

const postCursoInscricao = async (req, res, next) => {

  try {
    const { cursoid } = req.params;
    const body = req.body;
    const model = {
      curso_id: cursoid,
      name: body.name,
      email: body.email,
      data_nascimento: body.name,
    }
    await inscricoes.create(model);

    res.status(200).send({ message: 'Inscrição incluída com sucesso!' });

  } catch (error) {

    if (error instanceof BusinessError) 
      return res.status(error.statusCode).send({ message: error.message });

    return res.status().send({ message: error.message });

  }
}

const deleteCursoInscricao = async (req, res, next) => {
  try {

    const { idinscricao } = req.params;

    await inscricoes.destroy({
      where: {
        id: idinscricao,
      }
    });

    res.status(200).send({ message: 'Inscrição excluída com sucesso!' });

  } catch (error) {
    console.log(error);
    res.status(500).send({ message: 'Internal server error!' });
  }
}

const listarCursos = async (req, res, next) => {

  const result = await cursosServices.listarPorPerfilUsuario(req.usuario.tipo, req.usuario.id);

  return res.status(200).send(result);
}

const listarCursoPorId = async (req, res, next) => {

  try {
    const { params, usuario } = req;

    const result = await cursosServices.pesquisarCursoPorPerfilUsuario(params.cursoid, usuario.id, usuario.tipo);

    return res.status(200).send(result);

  } catch (error) {
    console.log(error);

    return res.status(500).send({ message: 'Internal server error!' });
  }
}

const criaCurso = async (req, res, next) => {

  try {
    console.log(req.body);

    const { name } = req.body;

    var resultNome = await cursosServices.validaSeNomeExiste(name);
    if (resultNome) {
      return res.status(400).send({
        mensagem: 'Já existe um curso com o nome informado.'
      });
    }

    await cursosServices.criaCurso(req.body);

    return res.status(200).send({
      mensagem: 'Curso criado com sucesso'
    });
  } catch (error) {

    console.log(error);

    return res.status(500).send({ message: 'Internal server error!!' });
  }
}

const atualizaCurso = async (req, res, next) => {

  try {
    const { cursoid } = req.params;

    await cursosServices.alteraCurso(cursoid, req.body);

    return res.status(200).send({
      mensagem: 'Curso alterado com sucesso'
    });
  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: 'Internal server error!!' });
  }
}

const deletarCurso = async (req, res, next) => {

  try {
    const { params } = req;
    const validationResult = await cursosServices.validaCursoExistente(params.cursoid)

    if (!validationResult)
      return res.status(422).send({
        mensage: 'O curso informado não existe!',
      });

    await cursosServices.deletaCurso(params.cursoid);

    return res.status(200).send({
      mensage: 'Operação realizada com sucesso',
    });
  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: 'Internal server error!!' });
  }
}

const realizarInscricao = async (req, res, next) => {

  try {
    const { params, usuario } = req;
    const validationResult = await cursosServices.validaCursoExistente(params.cursoid);
    
    if (!validationResult)
      throw new BusinessError('O curso informado não existe!');

    if (validationSubscription)
      return res.status(422).send({
        detalhes: ['Usuário já cadastrado no sistema.'],
      });

    await cursosServices.addInscricaoAluno(params.cursoid, usuario.id);

    return res.status(200).send({
      mensage: 'Sucesso!!',
    });

  } catch (error) {
    next(error);
  }
}

const removerInscricao = async (req, res, next) => {

  try {
    const { params, usuario } = req;

    const cursoValidation = await cursosServices.validaCursoExistente(params.cursoid);

    if (!cursoValidation)
      return res.status(422).send({
        detalhes: ['curso informado não existe'],
      });

    const inscricaoValidation = await inscricoesServices.validaSeInscricaoExiste(params.inscricaoid);

    if (!inscricaoValidation)
      return res.status(422).send({
        detalhes: ['inscrição informada não existe'],
      });

    console.log('params.inscricaoid: ', params.inscricaoid);

    const inscricaoUserValidation = await inscricoesServices.validaSeInscricaoPertenceAoUsuario(params.inscricaoid, usuario.id);
    if (!inscricaoUserValidation)
      return res.status(422).send({
        detalhes: ['Operação não realizada.'],
      });

    await inscricoesServices.removerInscricao(params.inscricaoid)

    return res.status(200).send({
      mensagem: 'Operação realizada com sucesso.'
    })

  } catch (error) {

    console.log(error);
    return res.status(500).send({ message: 'Internal server error!!' });
  }
}

module.exports = {
  atualizaCurso,
  criaCurso,
  listarCursos,
  deletarCurso,
  listarCursoPorId,
  realizarInscricao,
  removerInscricao,
  getCursos,
  getCursoPorId,
  postCursoInscricao,
  deleteCursoInscricao,
}
