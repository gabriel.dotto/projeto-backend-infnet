const cursosController = require('../controllers/inscricao.controller');

const { autorizar, validarDTO } = require('../utils/middlewares/dto-validator');

const Joi = require('joi').extend(require('@joi/date'));

module.exports = (router) => {

  router
    .route('/cursos')
    .get(
      autorizar(),
      cursosController.listarCursos,
    )
    .post(
      autorizar('CRIAR_CURSO'),
      validarDTO('body', {
        coordinator: Joi.string().min(5).required(),
        name: Joi.string().min(5).required(),
        start_date: Joi
          .date().format('DD/MM/YYYY')
          .required(),
      }),
      cursosController.criaCurso);

  router
    .route('/cursos/:idcurso')
    .get(
      autorizar(),
      validarDTO("params", {
        cursoid: Joi.number().integer().required().messages({
          'number.base': `o campo "idcurso" deve ser um número`,
          'any.required': `o campo "idcurso" é obrigatório`,
          'number.integer': `"idcurso" informe um número válido`
        })
      }),
      cursosController.listarCursoPorId,
    );

  router
    .route('/cursos/:idcurso')
    .put(
      autorizar('ALTERAR_CURSO'),
      validarDTO("params", {
        cursoid: Joi.number().integer().required().messages({
          'number.base': `o campo "idcurso" deve ser um número`,
          'any.required': `o campo "idcurso" é obrigatório`,
          'number.integer': `"idcurso" informe um número válido`
        })
      }),
      validarDTO("body", {
        name: Joi.string().required().messages({
          'any.required': `"nome" é um campo obrigatório`,
        }),
        start_date: Joi
          .date().format('DD/MM/YYYY')
          .required()
          .messages({
            'any.required': `"data_inicio" é um campo obrigatório`,
            'date.format': `"data_inicio" deve ser uma data válida "{#format}"`,
          }),
      }),
      cursosController.atualizaCurso
    )
    .delete(
      autorizar('DELETAR_CURSO'),
      validarDTO("params", {
        cursoid: Joi.number().integer().required().messages({
          'number.base': `o campo "idcurso" deve ser um número`,
          'any.required': `o campo "idcurso" é obrigatório`,
          'number.integer': `"idcurso" informe um número válido`
        })
      }),
      cursosController.deletarCurso,
    )

  router
    .route('/curso/:idcurso/inscricaocurso')
    .post(
      autorizar('REALIZAR_INSCRICAO'),
      validarDTO("params", {
        cursoid: Joi.number().integer().required().messages({
          'number.base': `o campo "idcurso" deve ser um número`,
          'any.required': `o campo "idcurso" é obrigatório`,
          'number.integer': `"idcurso" informe um número válido`
        })
      }),
      cursosController.realizarInscricao
    );

  router
    .route('/curso/:idcurso/inscricaocurso/:idinscricao')
    .delete(
      autorizar('DELETAR_INSCRICAO'),
      validarDTO("params", {
        cursoid: Joi.number().integer().required().messages({
          'any.required': `"idcurso" é um campo obrigatório`,
          'number.base': `"idcurso" deve ser um número`,
          'number.integer': `"idcurso" deve ser um número válido`
        }),
        inscricaoid: Joi.number().integer().required().messages({
          'any.required': `"idinscricao" é um campo obrigatório`,
          'number.base': `"idinscricao" deve ser um número`,
          'number.integer': `"idinscricao" deve ser um número válido`
        })
      }),
      cursosController.removerInscricao
    );
}
