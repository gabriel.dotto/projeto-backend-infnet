const { Router } = require("express");

const { name, version } = require('../../package.json');

const cursoRoutes = require('../routes/curso');
const usuarioRoutes = require('./alunos');

module.exports = (route) => {

  //Rota Raíz
  route.get('/', (req, res, next) => {
    res.send({ name, version });
  });

  const router = Router();

  cursoRoutes(router);
  usuarioRoutes(router);

  route.use('/v1', router);
}
