const usuarioController = require('../controllers/alunos.controller');
const { validarDTO, autorizar } = require('../utils/middlewares/dto-validator');

const Joi = require('joi').extend(require('@joi/date'));

module.exports = (router) => {

  router
    .route('/auth')
    .post(
      validarDTO('body', {
        usuario: Joi.string().required().messages({
            'any.required': `"Login" é um campo obrigatório`,
            'string.empty': `"Login" não deve ser vazio`,
          }),
        senha: Joi.string().required().messages({
            'any.required': `"Senha" é um campo obrigatório`,
            'string.empty': `"Senha" não deve ser vazio`,          
        }),
      }),

      usuarioController.autenticacao
    );

  router
    .route('/aluno')
    .get(
      autorizar('LISTAR_CURSO'),
      usuarioController.listarAlunos
    )
    .post(
      //autorizar(),
      validarDTO('body', {
        nome: Joi.string().min(5).required()
          .messages({
            'any.required': `"nome" é um campo obrigatório`,
            'string.empty': `"nome" não deve ser vazio`,
            'string.min': `"nome" não deve ter menos que {#limit} caracteres`,
          }),
        email: Joi.string().email().required().messages({
          'any.required': `"email" é um campo obrigatório`,
          'string.empty': `"email" não deve ser vazio`,
          'string.email': `"email" deve ser um email válido`,
        }),
        datanascimento: Joi
          .date().format('DD/MM/YYYY')
          .required()
          .messages({
            'any.required': `"datanascimento" é um campo obrigatório`,
            'date.format': `"datanascimento" deve ser uma data válida "{#format}"`,
          }),
        senha: Joi
          .string()
          .required()
          .min(6)
          .max(16)
          .messages({
            'any.required': `"senha" é um campo obrigatório`,
            'string.empty': `"senha" não deve ser vazio`,
            'string.min': `"senha" não deve ter menos que {#limit} caracteres`,
            'string.max': `"senha" não deve ter mais que {#limit} caracteres`,
          })
      }),
      usuarioController.cadastroAluno
    );

  router
    .route('/aluno/:id')
    .get(
      autorizar('LISTAR_CURSO'),
      usuarioController.pesquisaAlunoId
    )
    .put(
      autorizar('ALTERAR_ALUNO'),
      validarDTO('params', {
        id: Joi.number().integer().required().messages({
          'any.required': `"id" é um campo obrigatório`,
          'number.base': `"id" deve ser um número`,
          'number.integer': `"id" deve ser um número válido`
        })
      }),
      validarDTO('body', {
        nome: Joi.string().min(5).required()
          .messages({
            'any.required': `"nome" é um campo obrigatório`,
            'string.empty': `"nome" não deve ser vazio`,
            'string.min': `"nome" não deve ter menos que {#limit} caracteres`,
          }),
        email: Joi.string().email().required().messages({
          'any.required': `"email" é um campo obrigatório`,
          'string.empty': `"email" não deve ser vazio`,
          'string.email': `"email" deve ser um email válido`,
        }),
        datanascimento: Joi
          .date().format('DD/MM/YYYY')
          .required()
          .min(1900)
          .max(2003)
          .messages({
            'any.required': `"datanascimento" é um campo obrigatório`,
            'date.format': `Preencha o campo "datanascimento" com uma data válida "{#format}"`,
            'date.format': `Necessário ter mais que 18 anos.`,
        }),
        cpf: Joi
        .number()
        .integer()
        .min(11)
        .max(11)
        .required()
        .messages({
          'any.required': `"cpf" é um campo obrigatório`,
          'number.base': `"cpf" deve ser um número`,
          'number.integer': `"cpf" deve ser um número válido`
      })
      }),
      usuarioController.alteraAluno
    )

  router
    .route('/usuario')
    .get(
      autorizar('LISTAR_CURSO'),
      usuarioController.listarUsuarios
    );
}
