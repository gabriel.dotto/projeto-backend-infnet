const express = require('express');
const app = express();
const cors = require('cors');

const router = require('./routes/router');
const { errorHandler } = require('../src/utils/middlewares/dto-validator');

app.use(express.json());
app.use(cors());

router(app);

app.use(errorHandler());

const port = 3001;

app.listen(port, () => {
    
});

module.exports = app;
